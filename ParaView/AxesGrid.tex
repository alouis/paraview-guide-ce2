Oftentimes, you want to render a reference grid in the backgroud for a
visualization -- think axes in a chart view, except this time we are talking of the 3D
\ui{Render View}. Such a grid is useful to get an understanding for the data bounds and
placement in 3D space. In such cases, you use the \ui{Axes Grid}. \ui{Axes Grid} renders a 3D
grid with labels around the rendered scene. In this chapter, we will take a
closer look at using and customizing the \ui{Axes Grid}.

\section{The basics}

To turn on the \ui{Axes Grid} for a \ui{Render View}, you use the
\ui{Properties} panel. Under the \ui{View} section, you check the \ui{Axes
Grid} checkbox to turn the \ui{Axes Grid} on for the active view.

\begin{center}
\includegraphics[width=0.5\linewidth]{Images/EditAxesGrid.png}
\end{center}

Clicking on the \ui{Edit} button will pop up the \ui{Axes Grid}
properties dialog (Figure ~\ref{fig:EditAxesGridDialog})
that allows you to customize the \ui{Axes Grid}. As with the \ui{Properties}
panel, this is a searchable dialog, hence you can use the \ui{Search} box at the
top of the dialog to search of properties of interest. At the same time, the
\icon{Images/pqAdvanced26.png} button can be used to toggle between default and
advanced modes for the panel.

Using this dialog, you can change common properties like the titles (\ui{X
Title}, \ui{Y Title}, and \ui{Z Title}), title and label fonts using \ui{Title
Font Properties} and \ui{Label Font Properties} for each of the axes directions,
as well as the \ui{Grid Color}. Besides labelling the axes, you can render a
grid by checking \ui{Show Grid}. Once you have the \ui{Axes Grid} setup to your
liking, you can use the \icon{Images/SaveAsDefaultButton.png} to save your
selections so that they are automatically loaded next time you launch ParaView.
You can always use the \icon{Images/ReloadButton.png} to revert back to ParaView
defaults.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\linewidth]{Images/EditAxesGridDialog.png}
\caption{\ui{Edit Axes Grid} dialog is used to customize the \ui{Axes Grid}.}
\label{fig:EditAxesGridDialog}
\end{center}
\end{figure}

\section{Use cases}

To get a better look at the available customizations, let's look at various
visualizations possible and then see how you can set those up using the
properties on the \ui{Edit Axes Grid} dialog. In these examples, we use the
\texttt{disk\_out\_ref.ex2} example dataset distributed with ParaView.

\begin{center}
\includegraphics[width=0.4\linewidth]{Images/AxesGridBasic.png}
\hspace{1em}
\includegraphics[width=0.4\linewidth]{Images/AxesGridShowGrid.png}
\end{center}

In the images above, on the left is the default \ui{Axes Grid}. Simply turning on the visibility of the \ui{Axes
Grid} will generate such a visualization. The axes places always stay behind the
rendered geometry even as you interact with the scene. As you zoom in and out,
the labels and ticks will be updated based on visual cues.

Now to show a grid along the axes planes, alined with the ticks and labels, turn
on the \ui{Show Grid} checkbox, resulting in a visualization on the right.

By default, the gridded faces are always the fartest faces i.e. they stay behind
the rendered geometry and keep on updating as you rotate the scene. To fix which
faces of the bounding-box are to be rendered, use the \ui{Faces To Render}
button (it's an advanced property, so you may have to search for it using the
\ui{Seach} box in the \ui{Edit Axes Grid} dialog). Suppose, we want to label
just one face, the lower XY face. In that case, uncheck all the other faces
except \ui{Min-XY} in menu popped up on clicking on the \ui{Faces to Render}
button. This will indeed just show the min-XY face, however as you rotate the
scene, the face will get hidden as soon as the face gets closer to the camera
than the dataset. This is because, by default, \ui{Cull Frontfaces} is enabled.
Uncheck \ui{Cull Frontfaces} and ParaView will stop removing the face as it
comes ahead of the geometry, enabling a visualization as follows.

\begin{center}
\includegraphics[width=0.4\linewidth]{Images/AxesGridNoCulling.png}
\end{center}

Besides controlling which faces to render, you can also control where the labels
are placed. Let's say we want ParaView to decide how to place labels along the Y
axis, however for the X axis, we want to explicitly label the values $2.5$,
$0.5$, $-0.5$, and $-4.5$. To that, assuming we are the advanced mode for the
\ui{Edit Axes Grid} panel, check \ui{X Axis Use Custom Labels}.
That will show a table widget that allows you to add values as shown below.

\begin{center}
\includegraphics[width=0.4\linewidth]{Images/AxesGridCustomLabelsWidget.png}
\end{center}

Using the \icon{Images/pqPlus16.png} button, add the custom values.
While at it, let's also change the \ui{X Axis
Label Font Properties} and \ui{X Axis Title Font Properties} to change the color
to red and similar for the Y axis, let's change the color to green. Increase the
title font sizes to 18, to make them stand out and you will get a visualization
as follows (below, left).

\begin{center}
\includegraphics[width=0.4\linewidth]{Images/AxesGridCustomLabels.png}
\hspace{1em}
\includegraphics[width=0.4\linewidth]{Images/AxesGridCustomAxesToLabel.png}
\end{center}

Here we see that both sides of the axis plane are
labelled. Suppose you only want to label one of the sides, in that case use the
\ui{Axes To Label} property to uncheck all but \ui{Min-X} and \ui{Min-Y}. This
will result in the visualization shown above, right.

\section{\texttt{Axes Grid} in \texttt{pvpython}}

In \pvpython, \ui{Axes Grid} is accessible as the \py{AxesGrid} property on the
render view.

\begin{python}
>>> renderView = GetActiveView()

# AxesGrid property provides access to the AxesGrid object.
>>> axesGrid = renderView.AxesGrid

# To toggle visibility of the axes grid,
>>> axesGrid.Visibility = 1
\end{python}

All properties on the \ui{Axes Grid} that you set using the \ui{Edit Axes Grid}
dialog are avialble on this \py{axesGrid} object and can be changed as follows:

\begin{python}
>>> axesGrid.XTitle = 'X Title'
>>> axesGrid.XTitleColor = [0.6, 0.6, 0.0]
>>> axesGrid.XAxisLabels = [-0.5, 0.5, 2.5, 3.5]
\end{python}

Note you can indeed use the tracing capabilities described in
Section~\ref{sec:PythonTracing} to determine what Python API to use to change a
specific property on the \ui{Edit Axes Grid} dialog or use \py{help}.

\begin{python}
>>> help(axesGrid)
Help on GridAxes3DActor in module paraview.servermanager object:

class GridAxes3DActor(Proxy)
 |  GridAxes3DActor can be used to render a grid in a render view.
 |
 |  Method resolution order:
 |      GridAxes3DActor
 |      Proxy
 |      __builtin__.object
 |
 |  Methods defined here:
 |
 |  Initialize = aInitialize(self, connection=None, update=True)
 |
 |  ----------------------------------------------------------------------
 |  Data descriptors defined here:
 |
 |  AxesToLabel
 |      Set the mask to select the axes to label. The axes labelled will be a subset of the
 |      axes selected depending on which faces are also being rendered.
 |
 |  CullBackface
 |      Set to true to hide faces of the grid facing away from the camera i.e. hide all
 |      back faces.
 |
 |  CullFrontface
 |      Set to true to hide faces of the grid facing towards from the camera i.e. hide all
 |      front faces.
 |
 |  DataPosition
 |      If data is being translated, you can show the original data bounds for the axes
 |      instead of the translated bounds by setting the DataPosition to match the
 |      translation applied to the dataset.
 |
 ...
\end{python}
