% not sure why it's not picking up the icon command from LatexMacros.tex...
\providecommand\icon[1]{\includegraphics[height=1.em]{#1}}

This section describes ParaView Catalyst
from the perspective of the simulation user.
As described in the previous section,
Catalyst changes the workflow with the goal of
efficiently extracting useful insights during the
numerical simulation process.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=5in]{Images/differentworkflows.png}
\caption{Traditional workflow (blue) and ParaView Catalyst enhanced workflow (green).}
\label{fig:differentworkflows}
\end{center}
\end{figure}


With the ParaView Catalyst enhanced workflow, the user specifies
visualization and analysis output during the pre-processing step.
These output data are then generated during the simulation run and
later analyzed by the user. The Catalyst output can be
produced in a variety of formats such as rendered images with
pseudo-coloring of variables; plots (e.g. bar graphs, line plots, etc.);
data extracts (e.g. iso-surfaces, slices, streamlines, etc);
and computed quantities (e.g. lift on a wing, maximum stress, flow
rate, etc.). The goal of the enhanced workflow is to reduce the time to
gain insight into a given physical problem by performing some of the traditional post-processing
work \textit{in situ}. While the enhanced workflow uses ParaView Catalyst
to produce \textit{in situ} outputs,
the user does not need to be familiar with ParaView to use this functionality.
Configuration of the pre-processing
step can be based on generic information to produce desired
outputs (e.g. an iso-surface value and the variable to iso-surface) and the output can be written in either image
files or other formats with which the user has experience.

There are two major ways in which the user can utilize Catalyst for
\textit{in situ} analysis and visualization. The first is
to specify a set of parameters that are passed into a pre-configured
Catalyst pipeline. The second is to create a Catalyst pipeline script
using ParaView's GUI.

\section{Pre-Configured ParaView Catalyst Pipelines}
Creating pre-configured Catalyst pipelines places
more responsibility on the simulation developer but can simplify
matters for the user. Using pre-configured pipelines
can lower the barrier to using Catalyst with a simulation code.
The concept is that for most filters there is a
limited set of parameters that need to be set. For example, for a slice filter the user only needs
to specify a point and a normal defining the slice plane. Another example is for the threshold
filter where only the variable and range needs to be specified. For each pipeline though, the
parameters should also include a file name to output to and an output frequency. These
parameters can be presented for the user to set in their normal workflow for creating their
simulation inputs.

\section{Creating ParaView Catalyst Scripts in ParaView}\label{section:creatingscripts}
The downside to using pre-configured scripts is that they are only as useful as the simulation
developer makes them. These scripts can cover a large amount of use cases of interest to the
user but inevitably the user will want more functionality or better control. This is where it is
useful for the simulation user to create their own Catalyst Python scripts' pipeline using the
ParaView GUI.

There are two main prerequisites for creating Catalyst Python scripts in the ParaView GUI. The
first is that ParaView is built with the Catalyst Script Generator plugin enabled. This plugin was previously called
the CoProcessing Script Generator plugin for versions of ParaView before 4.2. Note that this plugin is
enabled by default when building ParaView from source as well as for versions of ParaView
installed from the available installers. Additionally, the version of ParaView used to generate the
script should also correspond to the version of ParaView Catalyst that the simulation code runs with. The
second prerequisite is that the user has a representative dataset to start from. What we mean
by this is that when reading the dataset from disk into ParaView that it is the same dataset type
(e.g. vtkUnstructuredGrid, vtkImageData, etc.) and has the same attributes defined over the
grids as the simulation adaptor code will provide to Catalyst during simulation runs. Ideally, the
geometry and the attribute ranges will be similar to what is provided by the simulation run's
configuration. The steps to create a Catalyst Python pipeline in the ParaView GUI are:
\begin{enumerate}
\item First load the ParaView plugin for creating the scripts. Do this by selecting ``Manage Plugins\ldots''
under the Tools menu (\menu{Tools > Manage Plugins\ldots}). In the window that pops up, select
CatalystScriptGeneratorPlugin and press the “Load Selected” button. After this, press the Close
button to close the window. This will create two new top-level menu items, \menu{Writers} and
\menu{CoProcessing}. Note that you can have the plugin automatically loaded when ParaView
starts up by expanding the CatalystScriptGeneratorPlugin information by clicking on the + sign in
the box to the left of it and then by checking the box to the right of Auto Load.
\item Next, load in a representative dataset and create a pipeline. In this case though, instead
of actually writing the desired output to a file we need to specify when and where the
files will be created when running the simulation. For data extracts we specify at this
point that information by choosing an appropriate writer under the \menu{Writers} menu. The
user should specify a descriptive file name as well as a write frequency in the Properties
panel as shown in Figure~\ref{fig:pipeline}. The file name must contain a \%t in it as this gets
replaced by the time step when creating the file. Note that the step to specify screenshot
outputs for Catalyst is done later.
\begin{figure}[htb]
\begin{center}
\includegraphics[width=3in]{Images/pipeline.png}
\caption{Example of a pipeline with one writer included. It writes output from
the Slice filter. Since the writer is selected its file name and write frequency properties are also shown.}
\label{fig:pipeline}
\end{center}
\end{figure}
\item Once the full Catalyst pipeline has been created, the Python script must be exported
from ParaView. This is done by choosing the Export State wizard under the
CoProcessing menu (\menu{CoProcessing > Export State}). The user can click on the Next button in the initial window that
pops up.
\item After that, the user must select the sources (i.e. pipeline objects without any input
connections) that the adaptor will create and add them to the output. Note that typically this
does not include sources from the \menu{Sources} menu since the generated Python
script will instantiate those objects as needed (e.g. for seeds for a streamline).
In the case shown in Figure~\ref{fig:pipeline} the source is the
filename\_10.pvti reader that is analogous to the input that the
simulation code's adaptor will provide. The user can either double click on the desired
sources in the left box to add them to the right box or select the desired sources in the
left box and click Add. This is shown in Figure~\ref{fig:sourceselector} below. After all of the proper sources
have been selected, click on Next.
\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in]{Images/sourceselector.png}
\caption{Selecting filename\_10.pvti as an input for the Catalyst pipeline.}
\label{fig:sourceselector}
\end{center}
\end{figure}
\item The next step is labeling the inputs. The most common case is a single input in which
case we use the convention that it should be named ``input'', the default value. For
situations where the adaptor can provide multiple sources (e.g. fluid-structure interaction
codes where a separate input exists for the fluid domain and the solid domain), the user
will need to label which input corresponds to which label. This is shown in Figure~\ref{fig:sourcelabelling}.
After this is done, click Next.
\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in]{Images/sourcelabelling.png}
\caption{Providing identifier strings for Catalyst inputs.}
\label{fig:sourcelabelling}
\end{center}
\end{figure}
\item The next page in the wizard gives the user the option to allow Catalyst to check for a
Live Visualization connection, to output screenshots from different views and Cinema output.
ParaView Live and Cinema are discussed in more detail in Sections~\ref{sec:paraviewlive} and \ref{sec:cinema},
respectively. For screenshots, there are a variety of
options. The first is a global option which will rescale the lookup table for pseudo-coloring
to the current data range for all views. The other options are per view and are:
\begin{itemize}
\item Image Type -- choice of image format to output the screenshot in.
\item File Name -- the name of the file to create. It must contain a \%t in it so that the
actual simulation time step value will replace it.
\item Write Frequency -- how often the screenshot should be created.
\item Magnification -- the user can create an image with a higher resolution than the
resolution shown in the current ParaView GUI view.
\item Fit to Screen -- specify whether to fit the data in the screenshot. This gives the
same results in Catalyst as clicking on the \icon{Images/fittoscreen.png}
%\includegraphics[scale=0.3]{Images/fittoscreen.png}
button in the ParaView GUI.
\end{itemize}
If there are multiple views, the user should toggle through each one with the Next View
and Previous View buttons in the window. After everything has been set, click on the
Finish button to create the Python script.
\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in,height=3.8in]{Images/imageoutput.png}
\caption{Setting the parameters for outputting screenshots.}
\label{fig:imageoutput}
\end{center}
\end{figure}
\item The final step is specifying the name of the generated Python script. Specify a directory
and a name to save the script at and click OK when finished.
\end{enumerate}

\subsection{Creating a Representative Dataset}
A question that often arises is how to create a representative dataset. There are two ways to do
this. The first way is to run the
simulation with Catalyst with a script that outputs the full grid with all attribute information.
Appendix~\ref{appendix:gridwriterscript} has a script that can be used for this purpose.
The second way is by using the sources and filters in ParaView.
The easiest grids to create within the GUI are image data grids (i.e. uniform rectilinear grids),
polydata and unstructured grids. For those knowledgeable enough about VTK, the
programmable source can also be used to create all grid types. If a multi-block grid is needed,
the Group Datasets filter can be used to group together multiple datasets into a single output.
The next step is to create the attribute information (i.e. point and/or cell data). This can be easily
done with the Calculator filter as it can create data with one or three components, name the
array to match the name of the array provided by the adaptor, and set an appropriate range of
values for the data. Once this is done, the user should save this out and then read the file back
in to have the reader act as the source for the pipeline.

\subsection{Manipulating Python Scripts}
For users that are comfortable programming in Python, we encourage them to modify the given
scripts as desired. The following information can be helpful for doing this:
\begin{itemize}
\item Sphinx generated ParaView Python API documentation at\\
\url{www.paraview.org/ParaView3/Doc/Nightly/www/py-doc/index.html}.
\item Using the ParaView GUI trace functionality to determine how to create desired filters and
set their parameters. This is done with \menu{Start Trace} and \menu{Stop Trace} under the \menu{Tools}
menu.
\item Using the ParaView GUI Python shell with tab completion. This is done with
\menu{Python Shell} under the \menu{Tools} menu.
\end{itemize}

\section{ParaView Live}
\label{sec:paraviewlive}
In addition to being able to set up pipelines \textit{a priori}, through
ParaView Live's capabilities the analyst can connect to the running simulation
through the ParaView GUI in order to modify existing pipelines.
This is useful for modifying the existing pipelines to improve
the quality of information coming out of a Catalyst enabled simulation.
The live connection is done through \menu{Catalyst > Connect\ldots}. This connects the simulation
to the pvserver where the data will be sent to. After the connection is made, the GUI's pipeline
will look like Figure~\ref{fig:catalystlivepipeline}. The live connection uses ParaView's concept
of not performing anything computationally expensive without specific prompting by the user.
Thus, by default none of the Catalyst extracts are sent to the server. This is indicated
by the \icon{Images/pqLinkIn16d.png} icon to the left of the pipeline sources. To have the output
from a source sent to the ParaView server, click on the \icon{Images/pqLinkIn16d.png} icon.
It will then change to \icon{Images/pqLinkIn16.png} to indicate that it is available on the
ParaView server. This is shown for Contour0 in Figure~\ref{fig:catalystlivepipeline}.
To stop the extract from being sent to the server, just delete the object in the ParaView
server's pipeline (e.g. Extract: Contour0 in Figure~\ref{fig:catalystlivepipeline}).

\begin{figure}[htb]
\begin{center}
\includegraphics[width=2.5in]{Images/pvguilivepipeline.png}
\caption{ParaView GUI pipeline for live connection.}
\label{fig:catalystlivepipeline}
\end{center}
\end{figure}

Beginning in ParaView 4.2, the live functionality was improved to allow the simulation
to also pause the Catalyst enabled simulation run. This is useful for examining the simulation
state at a specific point in time. The design is based on debugging tools such that the
simulation can be paused at the next available call to the Catalyst libraries, at a
specific time step or when the simulation time passes a specific value.
Additionally, a breakpoint that has not
been reached yet can be removed as well.
These controls are available under the \menu{Catalyst} menu. Note that the Pipeline
Browser shows the simulation run state to signify the status of the simulation. The icons for
this are:
\begin{itemize}
\item \icon{Images/pqInsituServerRunning16.png} indicates the simulation is running with no breakpoint set.
\item \icon{Images/pqInsituServerPaused16.png} indicates that the simulation has reached a breakpoint.
\item \icon{Images/pqInsituBreakpoint16.png} indicates that a breakpoint has been set but not yet reached.
\end{itemize}
A demonstration of this functionality is at \url{www.kitware.com/blog/home/post/722}.

\section{Cinema}
\label{sec:cinema}
Cinema is an image-based approach to \textit{in situ} analysis and visualization.
The concept is that by saving an organized set of images into a Cinema database,
an analyst can perform \textit{post hoc} analysis and visualization directly from the
generated images. ParaView Catalyst can be used to create a Cinema database
and the specification of the Cinema output can be done through the ParaView GUI's
Catalyst Script Generator plugin. Figure~\ref{fig:cinemaexport} shows the
expanded options when Output to Cinema is enabled. These options are:
\begin{itemize}
\item Export Type -- This option specifies how the view's camera should be manipulated
when generating the images. Current options include None, Static and Spherical. None indicates
that no Cinema output is requested for this view and Static indicates that the camera need not
be moved. Spherical will rotate the camera around the view's center at the given Phi and Theta angles.
\item Cinema Track Selection -- This option allows changing filter parameters and what field data to
pseudo-color with. By selecting the pipeline object in the left pane, users can specify the arrays
to pseudo-color by in the right pane's Arrays tab or the filter's parameters in the right pane's
Filter Values tab. Note that currently only Slice, Contour and Cut filters have been enabled
for modifying filter values.
\end{itemize}
See \url{www.cinemascience.org} or Ahrens, et. al.~\cite{ahrens2014a} for detailed information on Cinema.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=4in,height=5.8in]{Images/cinemaexport.png}
\caption{ParaView Catalyst export options for Cinema output.}
\label{fig:cinemaexport}
\end{center}
\end{figure}

\section{Avoiding Data Explosion}\label{section:avoidingdataexplosion}
A key point to keep in mind when creating Catalyst pipelines is that the choice and order of
filters can make a dramatic difference in the performance of Catalyst (this is true with
ParaView as well). Often, the source of
performance degradation is when dealing with very large amounts of data. For memory-limited
machines like today's supercomputers, poor decisions when creating a pipeline can cause the
executable to crash due to insufficient memory. The worst case scenario is creating an
unstructured grid from a topologically regular grid. This is because the filter will change from
using a compact grid data structure to a more general grid data structure.

We classify the filters into several categories, ordered from most memory efficient to least
memory efficient and list some commonly used filters for each category:
\begin{enumerate}
\item Total shallow copy or output independent of input -- negligible memory used in creating a
filter's output. The filters in this category are:
\begin{multicols}{3}
\begin{itemize}
\item Annotate Time
\item Append Attributes
\item Extract Block
\item Extract Datasets
\item Extract Level
\item Glyph
\item Group Datasets
\item Histogram
\item Integrate Variables
\item Normal Glyphs
\item Outline
\item Outline Corners
\item Pass Arrays
\item Plot Over Line
\item Probe Location
\end{itemize}
\end{multicols}
\item Add field data -- the same grid is used but an extra variable is stored. The filters in this category are:
\begin{multicols}{3}
\begin{itemize}
\item Block Scalars
\item Calculator\item Cell Data to Point Data\item Compute Derivatives\item Curvature\item Elevation
\item Generate Ids\item Generate Surface Normals\item Gradient\item Gradient of Unstructured DataSet\item Level Scalars\item Median\item Mesh Quality
\item Octree Depth Limit\item Octree Depth Scalars\item Point Data to Cell Data\item Process Id Scalars
\item Random Vectors\item Resample with Dataset\item Surface Flow\item Surface Vectors\item Transform
\item Warp (scalar)\item Warp (vector)
\end{itemize}
\end{multicols}
\item Topology changing, dimension reduction -- the output is a polygonal dataset but the output
cells are one or more dimensions less than the input cell dimensions. The filters in this category are:
\begin{multicols}{3}
\begin{itemize}
\item Cell Centers\item Contour\item Extract CTH Fragments\item Extract CTH Parts\item Extract Surface
\item Feature Edges\item Mask Points\item Outline (curvilinear)\item Slice\item Stream Tracer
\end{itemize}
\end{multicols}
\item Topology changing, moderate reduction -- reduces the total number of cells in the dataset
but outputs in either a polygonal or unstructured grid format. The filters in this category are:
\begin{multicols}{3}
\begin{itemize}
\item Clip
\item Decimate
\item Extract Cells by Region
\item Extract Selection
\item Quadric Clustering
\item Threshold
\end{itemize}
\end{multicols}
\item Topology changing, no reduction -- does not reduce the number of cells in the dataset while
changing the topology of the dataset and outputs in either a polygonal or unstructured grid
format. The filters in this category are:
\begin{multicols}{3}
\begin{itemize}
\item Append Datasets\item Append Geometry\item Clean\item Clean to Grid\item Connectivity\item D3\item Delaunay 2D/3D
\item Extract Edges\item Linear Extrusion\item Loop Subdivision\item Reflect\item Rotational Extrusion\item Shrink
\item Smooth\item Subdivide\item Tessellate\item Tetrahedralize\item Triangle Strips\item Triangulate
\end{itemize}
\end{multicols}
\end{enumerate}
When creating a pipeline, the filters should generally be ordered in this same fashion to limit
data explosion. For example, pipelines should be organized to reduce dimensionality early.
Additionally, reduction is preferred over extraction (e.g. the Slice filter is preferred over the Clip
filter). Extracting should only be done when reducing by an order of magnitude or more. When
outputting data extracts, subsampling (e.g. the Extract Subset filter or the Decimate filter) can
be used to reduce file size but caution should be used to make sure that the data reduction
doesn't hide any fine features. Additionally, the Pass Arrays filter can be used
to reduce the number of arrays that are passed through the
pipeline and potentially written to disk.

